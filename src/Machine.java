import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Machine {
    private JPanel root;
    private JLabel topLabel;
    private JLabel question;
    private JLabel items;
    private JButton friedUdonButton;
    private JButton kamatamaUdonButton;
    private JButton porkRamenButton;
    private JButton porkRiceButton;
    private JButton beefCurryButton1;
    private JButton hamburgRiceButton;
    private JButton halfBoiledEggButton;
    private JButton specialPlateButton;
    private JButton checkOutButton;
    private JLabel yen;
    private JLabel much;
    private JLabel total;
    private JTextPane receivedInfo;
    private JButton cancelButton;
    private String currentText;
    private int value = 0;
    private int confirmation;

    public Machine() {
        friedUdonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                confirmation = JOptionPane.showConfirmDialog(null, "Would you like to order Fried Udon?",
                        "Order Confirmation", JOptionPane.YES_NO_OPTION);

                if(confirmation == 0) {
                    currentText = receivedInfo.getText();
                    receivedInfo.setText(currentText + "Fried Udon (350yen)" + "\n");
                    value += 350;
                    much.setText(String.valueOf(value));

                    JOptionPane.showMessageDialog(null,
                            "Order for \"Fried Udon\" received.\nPlease wait a moment for arrival!");
                }
            }
        });
        kamatamaUdonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                confirmation = JOptionPane.showConfirmDialog(null, "Would you like to order Kamatama Udon?",
                        "Order Confirmation", JOptionPane.YES_NO_OPTION);

                if(confirmation == 0) {
                    currentText = receivedInfo.getText();
                    receivedInfo.setText(currentText + "Kamatama Udon (380yen)" + "\n");
                    value += 380;
                    much.setText(String.valueOf(value));

                    JOptionPane.showMessageDialog(null,
                            "Order for \"Kamatama Udon\" received.\nPlease wait a moment for arrival!");
                }
            }
        });
        beefCurryButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                confirmation = JOptionPane.showConfirmDialog(null, "Would you like to order Beef Curry?",
                        "Order Confirmation", JOptionPane.YES_NO_OPTION);

                if(confirmation == 0) {
                    currentText = receivedInfo.getText();
                    receivedInfo.setText(currentText + "Beef Curry (440yen)" + "\n");
                    value += 440;
                    much.setText(String.valueOf(value));

                    JOptionPane.showMessageDialog(null,
                            "Order for \"Beef Curry\" received.\nPlease wait a moment for arrival!");
                }
            }
        });
        halfBoiledEggButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                confirmation = JOptionPane.showConfirmDialog(null, "Would you like to order Half-boiled Egg?",
                        "Order Confirmation", JOptionPane.YES_NO_OPTION);

                if(confirmation == 0) {
                    currentText = receivedInfo.getText();
                    receivedInfo.setText(currentText + "Half-boiled Egg (70yen)" + "\n");
                    value += 70;
                    much.setText(String.valueOf(value));

                    JOptionPane.showMessageDialog(null,
                            "Order for \"Half-boiled Egg\" received.\nPlease wait a moment for arrival!");
                }
            }
        });
        porkRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                confirmation = JOptionPane.showConfirmDialog(null, "Would you like to order Pork Ramen?",
                        "Order Confirmation", JOptionPane.YES_NO_OPTION);

                if(confirmation == 0) {
                    currentText = receivedInfo.getText();
                    receivedInfo.setText(currentText + "Pork Ramen (500yen)" + "\n");
                    value += 500;
                    much.setText(String.valueOf(value));

                    JOptionPane.showMessageDialog(null,
                            "Order for \"Pork Ramen\" received.\nPlease wait a moment for arrival!");
                }
            }
        });
        porkRiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                confirmation = JOptionPane.showConfirmDialog(null, "Would you like to order Pork Rice?",
                        "Order Confirmation", JOptionPane.YES_NO_OPTION);

                if(confirmation == 0) {
                    currentText = receivedInfo.getText();
                    receivedInfo.setText(currentText + "Pork Rice (400yen)" + "\n");
                    value += 400;
                    much.setText(String.valueOf(value));

                    JOptionPane.showMessageDialog(null,
                            "Order for \"Pork Rice\" received.\nPlease wait a moment for arrival!");
                }
            }
        });
        hamburgRiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                confirmation = JOptionPane.showConfirmDialog(null, "Would you like to order Hamburg Rice?",
                        "Order Confirmation", JOptionPane.YES_NO_OPTION);

                if(confirmation == 0) {
                    currentText = receivedInfo.getText();
                    receivedInfo.setText(currentText + "Hamburg Rice (100yen)" + "\n");
                    value += 100;
                    much.setText(String.valueOf(value));

                    JOptionPane.showMessageDialog(null,
                            "Order for \"Hamburg Rice\" received.\nPlease wait a moment for arrival!");
                }
            }
        });
        specialPlateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                confirmation = JOptionPane.showConfirmDialog(null, "Would you like to order Special Plate?",
                        "Order Confirmation", JOptionPane.YES_NO_OPTION);

                if(confirmation == 0) {
                    currentText = receivedInfo.getText();
                    receivedInfo.setText(currentText + "Special Plate (1,500yen)" + "\n");
                    value += 1500;
                    much.setText(String.valueOf(value));

                    JOptionPane.showMessageDialog(null,
                            "Order for \"Special Plate\" received.\nPlease wait a moment for arrival!");
                }
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(value == 0)
                    JOptionPane.showMessageDialog(null,
                            "No order has been selected.\nPlease select the order you would like to order and press \"Check Out\" again.");
                else
                {
                    confirmation = JOptionPane.showConfirmDialog(null, "Would you like to checkout?",
                            "Checkout Confirmation", JOptionPane.YES_NO_OPTION);

                    if(confirmation == 0) {
                        JOptionPane.showMessageDialog(null,
                                "Thank you for your order. The total amount is " + value + " yen.\nPlease wait for a while until it arrives.");

                        receivedInfo.setText("");
                        value = 0;
                        much.setText(String.valueOf(value));
                    }
                }
            }
        });
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(value != 0)
                {
                    confirmation = JOptionPane.showConfirmDialog(null, "Would you like to cancel your orders?",
                                "Cancel Confirmation", JOptionPane.YES_NO_OPTION);

                    if(confirmation == 0) {
                        receivedInfo.setText("");
                        value = 0;
                        much.setText(String.valueOf(value));

                        JOptionPane.showMessageDialog(null,
                                "Canceled your order.\nPlease select your order again.");
                    }
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Machine");
        frame.setContentPane(new Machine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
